﻿using UnityEngine;
using System.Collections;
using System.Linq;
using UnityEngine.EventSystems;
using System.Collections.Generic;

public class DraggableLeftPanel : MonoBehaviour, IBeginDragHandler, IEndDragHandler, IDragHandler {
    private JobSystem _JobSystem;
    private Task _Task;
    private GameObject _MoveObject;
    private Transform _Panel;
    private Transform _ListParent;
    private int _Index = 0;
    private GameObject _OldSlot;

    public void Start ( ) {
        _JobSystem = JobSystem.Instance;
        _Task = transform.parent.gameObject.GetComponent<Task>();
        _ListParent = transform.parent;
        while(_ListParent.name != "List24") {
            _ListParent = _ListParent.parent;
        }
            
        _Panel = _ListParent.parent.parent.parent;
        
        //TODO: zrobić by zmienił rozmiar na domyślny 20px
    }

    //FEATURE zrobić by po najechaniu na liste dopasował się do elementu
    public void OnBeginDrag ( PointerEventData eventData ) {
        _OldSlot = GameObject.Instantiate(transform.parent.gameObject);
        _OldSlot.name = this.transform.parent.gameObject.name;
        var t = _OldSlot.GetComponent<Task>( );
        GameObject.Destroy(t);
        _OldSlot.transform.parent = _ListParent;
        var child = _OldSlot.transform.GetChild(0);
        var d = child.GetComponent<DraggableLeftPanel>( );
        GameObject.Destroy(d);
        child.GetChild(0).GetComponent<UnityEngine.UI.Text>( ).text = string.Empty;

        _MoveObject = transform.parent.gameObject;
        _Index = _MoveObject.transform.GetSiblingIndex( );
        _JobSystem.TableList[_Index] = _OldSlot;
        _OldSlot.transform.SetSiblingIndex(_Index);
        _MoveObject.transform.parent = _Panel;
        _MoveObject.GetComponent<CanvasGroup>( ).blocksRaycasts = false;
    }

    public void OnDrag ( PointerEventData eventData ) {
        _MoveObject.transform.position = eventData.position;

    }

    public void OnEndDrag ( PointerEventData eventData ) {

        var dropedObject = eventData.pointerCurrentRaycast.gameObject;
        var parent = dropedObject.transform.parent;

        while (parent != null) {
            if (parent.gameObject.tag == "Slot") break;
            parent = parent.parent;
        }

        if (parent == null) {

            var t = _MoveObject.GetComponent<Task>();
            _JobSystem.RemoveTask(t);
            GameObject.Destroy(t);
            t = null;
            GameObject.Destroy(_MoveObject);
            _MoveObject = null;
            _OldSlot = null;
            return;
        }

        var gO = parent.gameObject;
        _MoveObject.transform.parent = _ListParent;
        

        if (gO.GetComponent<Task>( )) {
            var indexOld = _OldSlot.transform.GetSiblingIndex();
            var indexGO = gO.transform.GetSiblingIndex( );
            _MoveObject.transform.SetSiblingIndex(indexGO);
            gO.transform.SetSiblingIndex(indexOld);
            GameObject.Destroy(_OldSlot);

            //TODO: wyliczenie godzin, zamiana task w _JobSystem.TaskList, 
            
            _JobSystem.TableList[indexOld] = gO;
            _JobSystem.TableList[indexGO] = _MoveObject;
        } else {
            //TODO: pamiętać o Task Liście i zmianie danych Task
            int index = gO.transform.GetSiblingIndex();
            _MoveObject.transform.SetSiblingIndex(index);
            _MoveObject.name = gO.name;
            GameObject.Destroy(gO);
            _JobSystem.TableList[index] = _MoveObject;
        }

        _MoveObject.GetComponent<CanvasGroup>( ).blocksRaycasts = true;
        _MoveObject = null;
        _OldSlot = null;
    }
}
