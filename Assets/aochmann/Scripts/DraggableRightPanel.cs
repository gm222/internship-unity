﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System;
using UnityEngine.UI;

public class DraggableRightPanel : MonoBehaviour, IBeginDragHandler, IEndDragHandler, IDragHandler {
    public GameObject newObject;
    public Transform _Panel;
    public Transform _LeftList;


    public void OnBeginDrag ( PointerEventData eventData ) {
        Debug.Log("Begin drag");

        newObject = Instantiate(this.gameObject);
        GameObject.Destroy(newObject.GetComponentInChildren<DraggableRightPanel>( ));
        var newObjectRectT = newObject.GetComponent<RectTransform>();
        var recT = this.GetComponent<RectTransform>();
        var taskLength = newObject.GetComponent<Task>().JobDuration;

        newObjectRectT.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, recT.rect.width);
        newObjectRectT.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, recT.rect.height * taskLength);

        newObject.GetComponent<CanvasGroup>().blocksRaycasts = false;
        Transform panel = transform.parent.parent.parent;

        newObject.transform.SetParent(panel);

    }

    public void OnDrag ( PointerEventData eventData ) {
        //Debug.Log("On drag");
        newObject.transform.position = eventData.position;
        //var dropedObject = eventData.pointerCurrentRaycast.gameObject;
    }

    public void OnEndDrag ( PointerEventData eventData ) {
        //var dropedObject = eventData.pointerCurrentRaycast.gameObject;
        //var parent = dropedObject.transform.parent;

        //while(parent != null) {
        //    if(parent.gameObject.tag == "Slot") break;
        //    parent = parent.parent;
        //}

        //if(parent == null) {
        //    GameObject.Destroy(newObject);
        //    newObject = null;
        //    return;
        //}
       
        //var newObjectTaskC = newObject.GetComponent<Task>();
        //var child          = parent.GetChild(0);
        //Task parentTask    = parent.gameObject.GetComponent<Task>( );

        //if (parentTask == null) {
        //    parentTask = parent.gameObject.AddComponent<Task>( );
        //    parentTask.JobDuration = Mathf.FloorToInt(parent.gameObject.GetComponent<UnityEngine.UI.LayoutElement>( ).preferredHeight / 20);
        //    child.gameObject.AddComponent<DraggableLeftPanel>( );
        //    JobSystem.Instance.AddTask(parentTask);

        //    //TODO: Zrobić tutaj dodawanie czasu rozpoczęcia i czasu zakończenia

        //}
        //parentTask.TaskName = newObjectTaskC.TaskName;
        //child.GetChild(0).GetComponent<UnityEngine.UI.Text>().text = parentTask.TaskName;

        //GameObject.Destroy(newObject);
        //newObject = null;
    }
}
