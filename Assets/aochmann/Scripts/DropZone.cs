﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

public class DropZone : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler {
    [SerializeField] Transform _Panel;
    private float _LastWidth = 0;
    private RectTransform _ObjectRectT;

    public void OnPointerEnter ( PointerEventData eventData ) {
        Debug.Log("OnBeginDrag");

        GameObject dragObject = eventData.pointerDrag;

        if (eventData.pointerDrag == null || eventData.pointerDrag.tag != "TaskSlot")
            return;

        
        _ObjectRectT = dragObject.GetComponent<RectTransform>();
        _LastWidth = _ObjectRectT.rect.width;
        dragObject.transform.SetParent(this.transform);
        _ObjectRectT.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, this.GetComponent<RectTransform>().rect.width);
    }

    public void OnPointerExit ( PointerEventData eventData ) {
        Debug.Log("OnEndDrag");

        GameObject dragObject = eventData.pointerDrag;
        if (eventData.pointerDrag == null)
            return;
        dragObject.transform.SetParent(_Panel);
        dragObject.GetComponent<RectTransform>( ).SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, _LastWidth);

    }
}