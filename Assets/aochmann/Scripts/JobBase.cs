﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public abstract class JobBase : MonoBehaviour {

    public static readonly Dictionary<string, int> __Jobs = new Dictionary<string, int>( ) {
        {"Ship Repair", 4 },
        {"Water Search", 3 },
        {"Patrol", 2 },
        {"Staff Training", 1 }
    };
    
    [SerializeField] private float _JobStart = 0f;
    [SerializeField] private float _JobEnd = 0f;
    [SerializeField] private float _JobDuration = 0f;
    [SerializeField] private string _TaskName;
    [SerializeField] private Action _Job;


    public JobBase ( ) {
        _JobDuration = 1f;
    }

    public JobBase (float jobStart, float jobEnd, string taskName, Action job = null ) {
        _JobStart = jobStart;
        _JobEnd = JobEnd;
        _TaskName = taskName;
        _Job = job;
    }


    public float JobStart {
        get {
            return _JobStart;
        }

        set {
            _JobStart = value;
        }
    }
    public float JobEnd {
        get {
            return _JobEnd;
        }

        set {
            _JobEnd = value;
        }
    }
    public string TaskName {
        get {
            return _TaskName;
        }

        set {
            _TaskName = value;
        }
    }
    public Action Job {
        get {
            return _Job;
        }

        set {
            _Job = value;
        }
    }

    public float JobDuration {
        get {
            return _JobDuration;
        }

        set {
            _JobDuration = value;
        }
    }
}
