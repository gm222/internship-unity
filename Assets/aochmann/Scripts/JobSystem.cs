﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class JobSystem : MonoSingleton<JobSystem> {
    [SerializeField] private List<JobBase> _JobList = new List<JobBase>( );
    [SerializeField] private GameObject _BackgroundSlot;
    [SerializeField] private Transform _BackgroundList;
    [SerializeField] private Transform _TaskListPanel;
    [SerializeField] private GameObject _TaskPrefab;
    [SerializeField] private Transform _TimeJobList;
    [SerializeField] private GameObject[] _TableList = new GameObject[24];
    [SerializeField] private GameObject _Slot;

    [SerializeField] private byte[] _HiddenObjects = new byte[24];

    private byte _JobTime = 24;

    public void Start ( ) {
        foreach (var item in JobBase.__Jobs) {
            var obj = GameObject.Instantiate(_TaskPrefab);
            var task = obj.AddComponent<Task>( );
            task.TaskName = item.Key;
            task.JobDuration = item.Value;
            var child = obj.transform.GetChild(0);
            child.GetChild(0).GetComponent<UnityEngine.UI.Text>( ).text = task.TaskName;
            child.GetChild(1).GetComponent<UnityEngine.UI.Text>( ).text = task.JobDuration.ToString();
            obj.transform.parent = _TaskListPanel;
        }

        for (int i = 0; i < 24; i++) {
            var obj = GameObject.Instantiate(_BackgroundSlot);
            obj.name = string.Format("B[{0}]", i);
            obj.transform.parent = _BackgroundList;
            obj.transform.GetChild(0).GetChild(0).gameObject.GetComponent<UnityEngine.UI.Text>( ).text = i.ToString();

            //obj = GameObject.Instantiate(_Slot);
            //obj.name = string.Format("Slot[{0}]", i+1);
            //obj.transform.parent =_TimeJobList;
            //_TableList[i] = obj;

        }

        var globalList = _BackgroundList.parent;
        var taskList = globalList.GetChild(1).gameObject.GetComponent<RectTransform>();
        var height = 24 * 20;
        taskList.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, height);
        

    }

    public void AddTask(Task t ) {
        _JobList.Add(t);
    }

    public void RemoveTask(Task t ) {
        _JobList.Remove(t);
    }

    public List<JobBase> JobList {
        get { return _JobList; }
        private set { _JobList = value; }
    }

    public GameObject[] TableList {
        get {
            return _TableList;
        }

        private set {
            _TableList = value;
        }
    }

    public byte JobTime {
        get {
            return _JobTime;
        }

        set {
            _JobTime = value;
        }
    }

    public byte[] HiddenObjects {
        get {
            return _HiddenObjects;
        }

        set {
            _HiddenObjects = value;
        }
    }
}
