﻿using UnityEngine;
using System.Collections;

public abstract class MonoSingleton<T> : MonoBehaviour {
    public static T Instance { get; private set; }

    private void Awake ( ) {
        if (Instance != null) {
            Debug.LogWarning("<color=#FF6622><b>Disabling script</b></color> '" + this.GetType( ).Name + "'. <b>Reason:</b> Multiple instances not allowed.", this);
            this.enabled = false;
            return;
        }
        Instance = this.GetComponent<T>( );
        OnAwake( );
    }

    protected virtual void OnDestroy ( ) {
        if (Instance != null && (object)Instance == (object)this.GetComponent<T>( ))
            Instance = default(T);
    }

    protected virtual void OnAwake ( ) { }
}