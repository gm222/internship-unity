﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System;
using UnityEngine.UI;

public class NewDragRight : MonoBehaviour, IBeginDragHandler, IEndDragHandler, IDragHandler {
    public GameObject newObject;
    private Transform _Panel;
    private RectTransform _SelfRec;

    public void Start ( ) {
        _Panel = transform.parent.parent.parent;
        _SelfRec = GetComponent<RectTransform>();
    }

    public void OnBeginDrag ( PointerEventData eventData ) {
        Debug.Log("Begin drag");

        newObject = Instantiate(this.gameObject);
        GameObject.Destroy(newObject.GetComponentInChildren<NewDragRight>( ));
        var newObjectRectT = newObject.GetComponent<RectTransform>( );
        var taskLength = newObject.GetComponent<Task>( ).JobDuration;

        newObjectRectT.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, _SelfRec.rect.width);
        newObjectRectT.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, _SelfRec.rect.height * taskLength);

        newObject.GetComponent<CanvasGroup>( ).blocksRaycasts = false;
        newObject.transform.SetParent(_Panel);

    }

    public void OnDrag ( PointerEventData eventData ) {
        //Debug.Log("On drag");
        Debug.Log("1");
        newObject.transform.position = eventData.position;
        //var dropedObject = eventData.pointerCurrentRaycast.gameObject;
    }

    public void OnEndDrag ( PointerEventData eventData ) {

        if (newObject.transform.parent == _Panel) {
            GameObject.Destroy(newObject);
            newObject = null;
            return;
        }

        var objectRect = newObject.GetComponent<RectTransform>( );
        objectRect.anchorMin = new Vector2(0, 1);
        objectRect.anchorMax = new Vector2(0, 1);
        objectRect.pivot = new Vector2(0, 1);
        


       
    }
}
