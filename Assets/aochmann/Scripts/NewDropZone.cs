﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

public class NewDropZone : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IDragHandler {
    
    RectTransform _SelfTransform;


    public void Start ( ) {
        _SelfTransform = GetComponent<RectTransform>();

        
    }
    float min = 0.1f;
    public void OnDrag ( PointerEventData eventData ) {

        //GameObject gO = eventData.pointerDrag;
        //NewDragRight drag;

        

        //if (gO == null || (drag = gO.GetComponent<NewDragRight>( )) == null) return;

        //Debug.Log("2");

        //var objectRect = drag.newObject.GetComponent<RectTransform>( );
        //objectRect.anchorMin = new Vector2(0, 1);
        //objectRect.anchorMax = new Vector2(0, 1);
        //objectRect.pivot = new Vector2(0, 1);

        //objectRect.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, _SelfTransform.rect.width);

        //objectRect.localPosition = new Vector3(0, 0);
    }

    public void OnPointerEnter ( PointerEventData eventData ) {
        GameObject gO = eventData.pointerDrag;
        NewDragRight drag;

        if (gO == null || (drag = gO.GetComponent<NewDragRight>( )) == null) return;

        drag.newObject.transform.parent = _SelfTransform;
        var objectRect = drag.newObject.GetComponent<RectTransform>();



        //objectRect.anchorMin = new Vector2(0,1);
        //objectRect.anchorMax = new Vector2(0, 1);
        //objectRect.pivot = new Vector2(0,1);

        objectRect.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, _SelfTransform.rect.width);

        //objectRect.localPosition = new Vector3(0,0);

        //objectRect.offsetMin = new Vector2(0, objectRect.offsetMin.y);

        //objectRect.sizeDelta = new Vector2(_SelfTransform.sizeDelta.x, objectRect.sizeDelta.y);

        // Left=position.x Right=sizeDelta.x PosY=position.y PosZ=position.z Height=sizeDelta.y


        //Debug.Log(string.Format("Previe OffsetMax:{0}\tOffsetMin:{1}", objectRect.offsetMax, objectRect.offsetMin));

        var rec = objectRect.rect;
        var l = rec.left;
        var r = rec.right;

        //objectRect.transform.position = new Vector3(objectRect.sizeDelta.x/2, -(objectRect.sizeDelta.y/2));

        
        //objectRect.offsetMax = new Vector2(0, objectRect.offsetMax.y);
        //objectRect.offsetMin = new Vector2(0, objectRect.offsetMin.y);

        //Debug.Log(string.Format("After OffsetMax:{0}\tOffsetMin:{1}", objectRect.offsetMax, objectRect.offsetMin));
        var left = objectRect.position.x;
        var right = objectRect.sizeDelta.x;
        var posY = objectRect.position.y;
        var PosZ = objectRect.position.z;
        var Height = objectRect.sizeDelta.y;
        //Debug.Log(string.Format("L:{0}\tR:{1}\tPY:{2}\tPZ:{3}\tH:{4}",left, right, posY, PosZ, Height));

    }

    public void OnPointerExit ( PointerEventData eventData ) {
        GameObject gO = eventData.pointerDrag;
        NewDragRight drag;

        if (gO == null ||( drag = gO.GetComponent<NewDragRight>( )) == null) return;

        drag.newObject.transform.parent = _SelfTransform.parent.parent.parent;


    }
}
