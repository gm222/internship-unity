﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System;
using UnityEditor;
using UnityEngine.UI;
using System.Linq;
using System.Collections.Generic;

public class ResizeAction : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IBeginDragHandler, IEndDragHandler, IDragHandler {
    private bool _ArrowVisable = false;
    private Vector2 _HotSpot;
    private static Texture2D __ArrowTexture;
    float _OldValue = 0f;
    private JobSystem _JobSystem;
    private Task _Task;
    private int _ElementId = 0;
    private GameObject _Parent;
    private LayoutElement _Layout;

    public void Start ( ) {
        _JobSystem = JobSystem.Instance;
        _Parent = transform.parent.gameObject;
        _Layout = _Parent.GetComponent<LayoutElement>();
        _Task = _Parent.GetComponent<Task>( );
        _ElementId = transform.parent.GetSiblingIndex(); 
    }
    

    public void Awake ( ) {
        if(__ArrowTexture == null) {
            //__ArrowTexture = 

            //TODO: Tutaj zrobić ladowanie textury


            _JobSystem = JobSystem.Instance;
            _Parent = transform.parent.gameObject;
            _Layout = _Parent.GetComponent<LayoutElement>( );
            _Task = _Parent.GetComponent<Task>( );
            _ElementId = transform.parent.GetSiblingIndex( );
        }
    }
    
    public void OnPointerEnter ( PointerEventData eventData ) {
        Cursor.visible = false;
        //_ArrowVisable = true;
        //Cursor.SetCursor(__ArrowTexture, _HotSpot, CursorMode.ForceSoftware);

    }

    public void OnPointerExit ( PointerEventData eventData ) {
        Cursor.visible = true;
        //_ArrowVisable = false;
        // Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);
    }

    public void OnBeginDrag ( PointerEventData eventData ) {
        _OldValue = _Layout.preferredHeight;
        _LastMousePosition = eventData.position.y;
        _CanSetUP = true;
        _CanSetDOWN = true;

    }

    public void OnEndDrag ( PointerEventData eventData ) {
        _CanSetUP = true;
        _CanSetDOWN = true;
    }
    bool _CanSetUP = true;
    bool _CanSetDOWN = true;
    float _LastMousePosition = 0;
    public void OnDrag ( PointerEventData eventData ) {
        if (eventData.position.y < _LastMousePosition) {
            //Debug.Log("Idziemy w dół");

            Debug.Log(string.Format("New = {0}\t Height = {1}", eventData.position.y, eventData.pressPosition.y - eventData.position.y));

            //if(_JobSystem.JobTime == 0) {
            //    _CanSetUP = true;
            //    return;
            //}

            int height = (int)(eventData.pressPosition.y - eventData.position.y)/20;

            if(height > 0) {
                int i = 0;
                for (i = _ElementId + 1; i <= height; i++) {
                    if (_JobSystem.TableList[i].GetComponent<Task>( ) != null)
                        break;
                    _JobSystem.TableList[i].SetActive(false);
                    _JobSystem.HiddenObjects[_ElementId]++;
                    _JobSystem.JobTime--;
                }

                _Layout.preferredHeight = _OldValue + (i-1)*20;
            }


            

            

            //if(height % 20 >= 7 && height % 20 < 20 && _CanSetUP) {
            //    if ((_ElementId + 1 + _JobSystem.HiddenObjects[_ElementId] >= 24) || _ElementId + 1 >= 24) return;
            //    if (_JobSystem.TableList[_ElementId + 1 + _JobSystem.HiddenObjects[_ElementId]].GetComponent<Task>( ) != null)
            //        return;

            //    _JobSystem.TableList[_ElementId + 1 + _JobSystem.HiddenObjects[_ElementId]].SetActive(false);
            //    _JobSystem.HiddenObjects[_ElementId]++;
            //    _JobSystem.JobTime--;
            //    _CanSetUP = false;
            //    //height = (int)height/20f;
            //    //height *= 20;
            //    _Layout.preferredHeight += 20;
            //}
            //else if(height % 20 == 0) {
            //    _CanSetUP = true;
            //}

            //if ((eventData.pressPosition.y - eventData.position.y) % 20 != 0) {
            //    _LastMousePosition = eventData.position.y;
            //    return;
            //}
            //if ((_ElementId + _JobSystem.HiddenObjects[_ElementId] >= 24) || _ElementId + 1 >= 24) return;
            //if (_JobSystem.TableList[_ElementId + 1 + _JobSystem.HiddenObjects[_ElementId]].GetComponent<Task>( ) != null)
            //    return;

            //_JobSystem.TableList[_ElementId + 1 + _JobSystem.HiddenObjects[_ElementId]].SetActive(false);
            //_JobSystem.HiddenObjects[_ElementId]++;
            //_JobSystem.JobTime--;




        } else if (eventData.position.y > _LastMousePosition) {

            //Debug.Log("Idziemy w górę");

            //if ((eventData.position.y - eventData.pressPosition.y) % 20 != 0) return;

            //if(_Layout.preferredHeight == 20f) return;

            //_JobSystem.TableList[_ElementId + _JobSystem.HiddenObjects[_ElementId]].SetActive(true);
            //_JobSystem.HiddenObjects[_ElementId]--;
            //_JobSystem.JobTime++;
            //_Layout.preferredHeight -= 20f;



            //if (_Layout.preferredHeight == 20f) return;

            //float height = eventData.position.y - eventData.pressPosition.y;

            //if (height % 20 >= 10 && height % 20 < 20 && _CanSetDOWN) {
            //    _JobSystem.TableList[_ElementId + _JobSystem.HiddenObjects[_ElementId]].SetActive(true);
            //    _JobSystem.HiddenObjects[_ElementId]--;
            //    _JobSystem.JobTime++;
            //    _CanSetDOWN = false;

            //    height = (int)height / 20f;
            //    height *= 20;
            //    _Layout.preferredHeight = _OldValue - height;

            //} else if (height % 20 == 0) {
            //    _CanSetDOWN = true;
            //}



        }

        _LastMousePosition = eventData.position.y;

        

        //var size = Mathf.RoundToInt((eventData.pressPosition.y - eventData.position.y) / 20) * 20;

        //if (_OldValue + size < 20f || (_OldValue + size)/20 > _JobSystem.JobTime) return;

        //var old = _Layout.preferredHeight;
        //var newValue = _OldValue + size;

        //if(old > newValue) {
        //    //Przywracanie
        //    _JobSystem.TableList[_ElementId + _JobSystem.HiddenObjects[_ElementId]].SetActive(true);
        //    _JobSystem.HiddenObjects[_ElementId]--;
        //    _JobSystem.JobTime++;
        //    _Layout.preferredHeight -= 20f;



        //    //działa ale wolno
        //    //int slotsToPerform = Mathf.FloorToInt((old - newValue)/20);

        //    //for (int i = _ElementId + _JobSystem.HiddenObjects[_ElementId], j = 0; i > _ElementId && j < slotsToPerform; i--, j++) {
        //    //    _JobSystem.TableList[i].SetActive(true);
        //    //    _JobSystem.HiddenObjects[_ElementId]--;
        //    //    _JobSystem.JobTime++;
        //    //    _Layout.preferredHeight -= 20f;
        //    //}

        //} else if(old < newValue && _JobSystem.JobTime > 0) {
        //    //ukrywanie
            
        //    if (_JobSystem.TableList[_ElementId + 1 + _JobSystem.HiddenObjects[_ElementId]].GetComponent<Task>( ) != null)
        //        return;

        //    _JobSystem.TableList[_ElementId + 1 + _JobSystem.HiddenObjects[_ElementId]].SetActive(false);
        //    _JobSystem.HiddenObjects[_ElementId]++;
        //    _JobSystem.JobTime--;
        //    _Layout.preferredHeight += 20f;


        //    //działa ale wolno
        //    //int slotsToPerform = Mathf.FloorToInt((newValue - _OldValue) / 20) - _JobSystem.HiddenObjects[_ElementId];

        //    //for (int j = 0; j < slotsToPerform; j++) {
        //    //    if (_JobSystem.TableList[_ElementId + 1 + j].GetComponent<Task>( ) != null) break;
        //    //    if (_JobSystem.TableList[_ElementId + 1 + j].active) {
        //    //        _JobSystem.TableList[_ElementId + 1 + j].SetActive(false);
        //    //        _JobSystem.HiddenObjects[_ElementId]++;
        //    //        _JobSystem.JobTime--;
        //    //        _Layout.preferredHeight+=20f;
        //    //    }
        //    //}

        //}

        //if(old > testGame.preferredHeight) {
        //    //przywracanie
        //    var count = Mathf.FloorToInt((old - testGame.preferredHeight)/20);
        //    for (int i = _ListHideItems.Count-1, j = 0; i >=0 && j < count; i--, j++) {
        //        _JobSystem.TableList[_ListHideItems[i]].SetActive(true);
        //        _ListHideItems.RemoveAt(i);
        //        _Hided--;
        //        _JobSystem.JobTime++;
        //    }
        //} else if(old < testGame.preferredHeight && _JobSystem.JobTime > 0) {
        //    //ukrywanie
        //    int slotsToPerform = Mathf.FloorToInt(testGame.preferredHeight / 20) - 1 - _Hided;
        //    for (int i = _ElementId, j =0; i < 24 && j < slotsToPerform; i++) {
        //        if (_JobSystem.TableList[i].active && _JobSystem.TableList[i].GetComponent<Task>() == null) {
        //            _JobSystem.TableList[i].SetActive(false);
        //            _ListHideItems.Add(i);
        //            j++;
        //            _Hided++;
        //            _JobSystem.JobTime--;
        //        }
        //    }

        //}


    }




    //public void OnGUI ( ) {
    //    if (_ArrowVisable) {
    //        EditorGUIUtility.AddCursorRect(new Rect(new Vector2(Input.mousePosition.x, Input.mousePosition.y), new Vector2(20,20)), MouseCursor.MoveArrow);
    //    }
    //}
}
